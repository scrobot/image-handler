<?php

namespace Yadeshevle\ImageHandler;

use Illuminate\Support\ServiceProvider;

class ImageHandlerServiceProvider extends ServiceProvider {

    public function register()
    {

        $this->mergeConfigFrom(
            realpath(__DIR__ . '/../config/config.php'), 'module'
        );

    }

    public function boot()
    {

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'image_handler');

        $this->publishes([
            __DIR__.'/../migrations/' => database_path('migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../config/' => config_path(),
        ], 'config');

        $this->publishes([
            __DIR__.'/../public/' => public_path()
        ], 'public');

        require __DIR__."/routes.php";
        require __DIR__."/blade.php";

        pusher('module_style', 'image_handler::styles');
        pusher('scripts', 'image_handler::scripts');


    }

}