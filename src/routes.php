<?php

Route::get('/handlers/images/display/{_session}', '\Yadeshevle\ImageHandler\ImageHandlerController@getDisplay');

Route::group(['prefix' => 'handlers', 'middleware' => 'auth.staff'], function () {
    Route::controllers([
        'images' => '\Yadeshevle\ImageHandler\ImageHandlerController'
    ]);
});
