<?php

namespace Yadeshevle\ImageHandler;

trait ImageableTrait {

    public function images()
    {
        return $this->morphMany('Yadeshevle\ImageHandler\ImageHandler', 'imageable');
    }

    public function preview($thumb = null)
    {
        $image = $this->images->where('preview', 1)->first();

        if(is_null($image)) {
            return "/images/no_img.jpg";
        }

        if(!is_null($thumb)) {
            return $image->preferences[$thumb];
        }

        return $image->preferences['preview'];
    }

    public function session_token()
    {
        $image = $this->images->first();
        return $image->session_token;
    }

}