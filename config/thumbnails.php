<?php

require_once __DIR__."/../core/system_helpers/helpers.php";

return [
    /*
     "thumb_key_name" => [ // thumb options Array
        'width' => '150',
        'height' => '150',
        'path' => loc_uploads_path().'/some_folder",
    ]
     */

    'portfolio' => [

        "120x120" => [
            'width' => 120,
            'height' => 120,
            'path' => upload_path().'/portfolio/120x120/',
            'abs_path' => uploads().'/portfolio/120x120/',
        ],

        "150x150" => [
            'width' => 150,
            'height' => 150,
            'path' => upload_path().'/portfolio/150x150/',
            'abs_path' => uploads().'/portfolio/150x150/',
        ],

        "192x110" => [
            'width' => 192,
            'height' => 110,
            'path' => upload_path().'/portfolio/192x110/',
            'abs_path' => uploads().'/portfolio/192x110/',
        ],

    ],

    'products' => [

        "120x120" => [
            'width' => 120,
            'height' => 120,
            'path' => upload_path().'/portfolio/120x120/',
            'abs_path' => uploads().'/portfolio/120x120/',
        ],

        "128x145" => [
            'width' => 128,
            'height' => 145,
            'path' => upload_path().'/products/128x145/',
            'abs_path' => uploads().'/products/128x145/',
        ],

        "182x120" => [
            'width' => 182,
            'height' => 120,
            'path' => upload_path().'/products/182x120/',
            'abs_path' => uploads().'/products/182x120/',
        ]

    ],



];