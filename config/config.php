<?php
return [
    'image-handler' => [
        'name' => 'Обработчик изображений',
        'description' => 'Вспомогательный модуль для обработки изображений.',
        'version' => 'dev-master',
        'package' => 'core',
    ]
];